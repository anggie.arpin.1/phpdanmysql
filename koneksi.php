<!--Ini adalah file yang berfungsi sebagai penghubung PHP dengan MySQL -->
<!--Saya merancangnya mirip seperti file .env di Laravel, supaya mudah tinggal panggil di file lain saja-->
<?php 
    $servername = "localhost";   // nama servername
    $username = "root";  // nama default DB
    $password = "";       // password Default
    $dbname = "si_dosen";  //Ini nama DB yang digunakan pada sistem

    $conn = mysqli_connect($servername, $username, $password, $dbname); //variabel yang digunakan untuk koneksi

    if($conn){
        
    }else{
        die("Connection failed : ".mysqli_connect_error()); // Kalau gagal terhubung muncul pesan ini
    }
?>